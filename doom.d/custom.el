(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(akater-misc-privileged-tramp-method "doas")
 '(auth-source-save-behavior nil)
 '(custom-safe-themes
   '("d0fd069415ef23ccc21ccb0e54d93bdbb996a6cce48ffce7f810826bb243502c" "8f5b54bf6a36fe1c138219960dd324aad8ab1f62f543bed73ef5ad60956e36ae" "e9d47d6d41e42a8313c81995a60b2af6588e9f01a1cf19ca42669a7ffd5c2fde" default))
 '(pdf-info-epdfinfo-program "/usr/bin/epdfinfo")
 '(send-mail-function 'mailclient-send-it)
 '(smtpmail-smtp-server "imap.gmail.com")
 '(smtpmail-smtp-service 25))
(put 'downcase-region 'disabled nil)
(put 'customize-face 'disabled nil)
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(vterm-color-blue ((t (:inherit vterm-color-white)))))
(put 'customize-group 'disabled nil)
(put 'customize-variable 'disabled nil)
